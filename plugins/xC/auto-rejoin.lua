--
-- auto-rejoin.lua: join back automatically when someone kicks you
--
-- Copyright (c) 2016, Přemysl Eric Janouch <p@janouch.name>
--
-- Permission to use, copy, modify, and/or distribute this software for any
-- purpose with or without fee is hereby granted.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
-- SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
-- OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
-- CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

local timeout
xC.setup_config {
	timeout = {
		type = "integer",
		comment = "auto rejoin timeout",
		default = "0",

		on_change = function (v)
			timeout = v
		end,
		validate = function (v)
			if v < 0 then error ("timeout must not be negative", 0) end
		end,
	},
}

async, await = xC.async, coroutine.yield
xC.hook_irc (function (hook, server, line)
	local msg = xC.parse (line)
	if msg.command ~= "KICK" then return line end

	local who = msg.prefix:match ("^[^!]*")
	local channel, whom = table.unpack (msg.params)
	if who ~= whom and whom == server.user.nickname then
		async.go (function ()
			await (async.timer_ms (timeout * 1000))
			server:send ("JOIN " .. channel)
		end)
	end
	return line
end)
