--
-- prime.lua: highlight prime numbers in messages
--
-- Copyright (c) 2020, Přemysl Eric Janouch <p@janouch.name>
--
-- Permission to use, copy, modify, and/or distribute this software for any
-- purpose with or without fee is hereby granted.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
-- SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
-- OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
-- CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

local smallest, highlight = 0, "\x1f"
xC.setup_config {
	smallest = {
		type = "integer",
		default = "0",
		comment = "smallest number to scan for primality",
		on_change = function (v) smallest = math.max (v, 2) end
	},
	highlight = {
		type = "string",
		default = "\"\\x1f\"",
		comment = "the attribute to use for highlights",
		on_change = function (v) highlight = v end
	},
}

-- The prime test is actually very fast, so there is no DoS concern
local do_intercolour = function (text)
	return tostring (text:gsub ("%f[%w_]%d+", function (n)
		if tonumber (n) < smallest then return nil end
		for i = 2, n ^ (1 / 2) do if (n % i) == 0 then return nil end end
		return highlight .. n .. highlight
	end))
end

local do_interlink = function (text)
	local rebuilt, last = {""}, 1
	for start in text:gmatch ('()\x03') do
		table.insert (rebuilt, do_intercolour (text:sub (last, start - 1)))
		local sub = text:sub (start + 1)
		last = start + (sub:match ('^%d%d?,%d%d?()') or sub:match ('^%d?%d?()'))
		table.insert (rebuilt, text:sub (start, last - 1))
	end
	return table.concat (rebuilt) .. do_intercolour (text:sub (last))
end

local do_message = function (text)
	local rebuilt, last = {""}, 1
	for run, link, endpos in text:gmatch ('(.-)(%f[%g]https?://%g+)()') do
		last = endpos
		table.insert (rebuilt, do_interlink (run) .. link)
	end
	return table.concat (rebuilt) .. do_interlink (text:sub (last))
end

-- XXX: sadly it won't typically highlight primes in our own messages,
-- unless IRCv3 echo-message is on
xC.hook_irc (function (hook, server, line)
	local start, message = line:match ("^(.- PRIVMSG .- :)(.*)$")
	return message and start .. do_message (message) or line
end)
