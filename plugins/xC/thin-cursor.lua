--
-- thin-cursor.lua: set a thin cursor
--
-- Copyright (c) 2016, Přemysl Eric Janouch <p@janouch.name>
--
-- Permission to use, copy, modify, and/or distribute this software for any
-- purpose with or without fee is hereby granted.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
-- SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
-- OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
-- CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--
-- If tmux doesn't work, add the following to its configuration:
--   set -as terminal-overrides ',*:Ss=\E[%p1%d q:Se=\E[2 q'
-- Change the "2" as per http://invisible-island.net/xterm/ctlseqs/ctlseqs.html

local out = io.output ()
out:write ("\x1b[6 q"):flush ()

-- By registering a global variable, we get notified about plugin unload
x = setmetatable ({}, { __gc = function ()
	out:write ("\x1b[2 q"):flush ()
end })
