--
-- ping-timeout.lua: ping timeout readability enhancement plugin
--
-- Copyright (c) 2015 - 2016, Přemysl Eric Janouch <p@janouch.name>
--
-- Permission to use, copy, modify, and/or distribute this software for any
-- purpose with or without fee is hereby granted.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
-- SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
-- OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
-- CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

xC.hook_irc (function (hook, server, line)
	local msg = xC.parse (line)
	local start, timeout = line:match ("^(.* :Ping timeout:) (%d+) seconds$")
	if msg.command ~= "QUIT" or not start then
		return line
	end

	local minutes = timeout // 60
	if minutes == 0 then
		return line
	end

	local seconds = timeout % 60
	return ("%s %d minutes, %d seconds"):format (start, minutes, seconds)
end)
