#!/usr/bin/env perl
# Creates a database for the "seen" plugin from logs for xC.
# The results may not be completely accurate but are good for jumpstarting.
# Usage: ./seen-import-xC.pl LOG-FILE... > seen.db

use strict;
use warnings;
use File::Basename;
use Time::Piece;

my $db = {};
for (@ARGV) {
	my $where = (basename($_) =~ /\.(.*).log/)[0];
	unless ($where) {
		print STDERR "Invalid filename: $_\n";
		next;
	}

	open my $fh, '<', $_ or die "Failed to open log file: $!";
	while (<$fh>) {
		my ($when, $who, $who_action, $what) =
			/^(.{19}) (?:<[~&@%+]*(.*?)>| \*  (\S+)) (.*)/;
		next unless $when;

		if ($who_action) {
			$who = $who_action;
			$what = "* $what";
		}
		$db->{$who}->{$where} =
			[Time::Piece->strptime($when, "%Y-%m-%d %T")->epoch, $what];
	}
}

while (my ($who, $places) = each %$db) {
	while (my ($where, $data) = each %$places) {
		my ($when, $what) = @$data;
		print ":$who PRIVMSG $where $when :$what\n";
	}
}
