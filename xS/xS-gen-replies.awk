#!/usr/bin/awk -f
/^[0-9]+ *(ERR|RPL)_[A-Z]+ *".*"$/ {
	match($0, /".*"/)
	ids[$1] = $2
	texts[$2] = substr($0, RSTART, RLENGTH)
}

END {
	print "package main"
	print ""
	print "const ("
	for (i in ids)
		printf("\t%s = %s\n", ids[i], i)
	print ")"
	print ""
	print "var defaultReplies = map[int]string{"
	for (i in ids)
		print "\t" ids[i] ": " texts[ids[i]] ","
	print "}"
}
