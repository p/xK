#!/usr/bin/env wdye
-- Very basic end-to-end testing for CI
function exec (...)
	local p = wdye.spawn(...)
	local out = wdye.expect(p:eof {function (p) return p[0] end})
	if not out then
		error "exec() timeout"
	end

	local status = p:wait()
	if status ~= 0 then
		io.write(out, "\n")
		error("exit status " .. status)
	end
	return out:gsub("%s+$", "")
end

local temp = exec {"mktemp", "-d"}
local atexit = {}
setmetatable(atexit, {__gc = function () exec {"rm", "-rf", "--", temp} end})

local env = {XDG_CONFIG_HOME=temp, TERM="xterm"}
exec {"./xD", "--write-default-cfg", environ=env}

-- Run the daemon to test against (assuming the default port 6667)
local xD = wdye.spawn {"./xD", "-d", environ=env}
local xC = wdye.spawn {"./xC", environ=env}

function send (...) xC:send(...) end
function expect (string)
	wdye.expect(xC:exact {string},
		wdye.timeout {5, function (p) error "xC timeout" end},
		xC:eof {function (p) error "xC exited prematurely" end})
end

-- Connect to the daemon
send "/server add localhost\n"
expect "]"
send "/set servers.localhost.addresses = \"localhost\"\n"
expect "Option changed"
send "/disconnect\n"
expect "]"
send "/connect\n"
expect "Welcome to"

-- Try some chatting
send "/join #test\n"
expect "has joined"
send "Hello\n"
expect "Hello"

-- Attributes
send "\x1bmbBold text! \x1bmc0,5And colors.\n"
expect "]"

-- Try basic commands
send "/set\n"
expect "]"
send "/help\n"
expect "]"

-- Quit
send "/quit\n"
expect "Shutting down"

local s1 = xC:wait()
assert(s1 == 0, "xC exited abnormally: " .. s1)

-- Send SIGINT (^C)
xD:send "\003"
local s2 = xD:wait()
assert(s2 == 0, "xD exited abnormally: " .. s2)
