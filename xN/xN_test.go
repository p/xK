//
// Copyright (c) 2024, Přemysl Eric Janouch <p@janouch.name>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//

package main

import "testing"

func TestParseURL(t *testing.T) {
	for _, rawURL := range []string{
		`irc:server/channel`,
		`irc://server/channel,isnetwork`,
		`ircs://ssl.ircnet.io`,
		`ircs://ssl.ircnet.io/`,
		`http://server/path`,
	} {
		if _, _, err := parse(rawURL, nil); err == nil {
			t.Errorf("%q should not parse\n", rawURL)
		}
	}

	for _, test := range []struct {
		rawURL string
		p      parameters
	}{
		{
			rawURL: `irc://uptime@localhost/%23watch?skipjoin&usenotice`,
			p: parameters{
				username:  "uptime",
				target:    "#watch",
				skipjoin:  true,
				usenotice: true,
			},
		},
		{
			rawURL: `ircs://ohayou@irc.libera.chat/john,isuser,isserver`,
			p: parameters{
				username: "ohayou",
				target:   "john",
				isuser:   true,
			},
		},
		{
			rawURL: `ircs://agent:Password123@irc.cia.gov:1337/#hq?key=123456`,
			p: parameters{
				username: "agent",
				password: "Password123",
				target:   "#hq",
				chankey:  "123456",
			},
		},
	} {
		p, _, err := parse(test.rawURL, nil)
		if err != nil {
			t.Errorf("%q should parse, got: %s\n", test.rawURL, err)
			continue
		}
		if p.username != test.p.username {
			t.Errorf("%q: username: %v ≠ %v\n",
				test.rawURL, p.username, test.p.username)
		}
		if p.password != test.p.password {
			t.Errorf("%q: password: %v ≠ %v\n",
				test.rawURL, p.password, test.p.password)
		}
		if p.target != test.p.target {
			t.Errorf("%q: target: %v ≠ %v\n",
				test.rawURL, p.target, test.p.target)
		}
		if p.isuser != test.p.isuser {
			t.Errorf("%q: isuser: %v ≠ %v\n",
				test.rawURL, p.isuser, test.p.isuser)
		}
		if p.chankey != test.p.chankey {
			t.Errorf("%q: chankey: %v ≠ %v\n",
				test.rawURL, p.chankey, test.p.chankey)
		}
		if p.skipjoin != test.p.skipjoin {
			t.Errorf("%q: skipjoin: %v ≠ %v\n",
				test.rawURL, p.skipjoin, test.p.skipjoin)
		}
		if p.usenotice != test.p.usenotice {
			t.Errorf("%q: usenotice: %v ≠ %v\n",
				test.rawURL, p.usenotice, test.p.usenotice)
		}
	}
}
