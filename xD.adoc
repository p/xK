xD(1)
=====
:doctype: manpage
:manmanual: xK Manual
:mansource: xK {release-version}

Name
----
xD - IRC daemon

Synopsis
--------
*xD* [_OPTION_]...

Description
-----------
*xD* is a basic IRC daemon for single-server networks, suitable for testing
and private use.  When run without a configuration file, it will start listening
on the standard port 6667 and the "any" address.

Options
-------
*-d*, *--debug*::
	Do not daemonize, print more information on the standard error stream
	to help debug various issues.

*-h*, *--help*::
	Display a help message and exit.

*-V*, *--version*::
	Output version information and exit.

*--write-default-cfg*[**=**__PATH__]::
	Write a configuration file with defaults, show its path and exit.
+
The file will be appropriately commented.
+
When no _PATH_ is specified, it will be created in the user's home directory,
contrary to what you might expect from a server.

Files
-----
*xD* follows the XDG Base Directory Specification.

_~/.config/xD/xD.conf_::
_/etc/xdg/xD/xD.conf_::
	The daemon's configuration file.  Use the *--write-default-cfg* option
	to create a new one for editing.

Reporting bugs
--------------
Use https://git.janouch.name/p/xK to report bugs, request features,
or submit pull requests.
