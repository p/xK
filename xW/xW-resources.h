#define IDI_ICON                1
#define IDI_HIGHLIGHTED         2
#define IDR_BEEP                3
#define IDA_ACCELERATORS       10

// Named after input_add_functions() in xC.
#define ID_PREVIOUS_BUFFER     11
#define ID_NEXT_BUFFER         12
#define ID_SWITCH_BUFFER       13
#define ID_GOTO_HIGHLIGHT      14
#define ID_GOTO_ACTIVITY       15
#define ID_TOGGLE_UNIMPORTANT  16
#define ID_DISPLAY_FULL_LOG    17

#define IDD_CONNECT            20
#define IDC_STATIC             21
#define IDC_HOST               22
#define IDC_PORT               23
