#!/usr/bin/awk -f
BEGIN {
	# The message catalog is a by-product
	msg = "xD.msg"
	print "$quote \"" > msg
	print "$set 1" > msg
}

/^[0-9]+ *IRC_(ERR|RPL)_[A-Z]+ *".*"$/ {
	match($0, /".*"/)
	ids[$1] = $2
	texts[$2] = substr($0, RSTART, RLENGTH)
	print $1 " " texts[$2] > msg
}

END {
	printf("enum\n{")
	for (i in ids) {
		if (seen_first)
			printf(",")
		seen_first = 1
		printf("\n\t%s = %s", ids[i], i)
	}
	print "\n};\n"
	print "static const char *g_default_replies[] =\n{"
	for (i in ids)
		print "\t[" ids[i] "] = " texts[ids[i]] ","
	print "};"
}
